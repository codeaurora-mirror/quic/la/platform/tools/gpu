{{/*
 * Copyright (C) 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */}}

{{define "Go.Client"}}
  {{$s := .Service}}
  {{File.Import "android.googlesource.com/platform/tools/gpu/binary/registry"}}
  {{File.Import "android.googlesource.com/platform/tools/gpu/log"}}
  {{File.Import "android.googlesource.com/platform/tools/gpu/multiplexer"}}
  {{File.Import "android.googlesource.com/platform/tools/gpu/rpc"}}
  {{range $s.Methods}}
    {{range .Call.Params}}{{Call "Go.Import" .Type}}{{end}}
    {{range .Result.List}}{{Call "Go.Import" .Type}}{{end}}
  {{end}}
  {{template "Go.Prelude" .}}
  ¶
  // Client is the client interface for {{$s.Name}} calls.¶
  type Client interface {»¶
    // Client exposes all the {{$s.Name}} interface methods.¶
    {{$s.Name}}¶
    // Multiplexer returns the multiplexer used for communication to the server.¶
    Multiplexer() *multiplexer.Multiplexer¶
    // Namespace returns the custom namespace used for decoding responses from the¶
    // server, or nil if no custom namespace has been specified.¶
    Namespace() *registry.Namespace¶
  «}¶
  type client struct{ rpc.Client }¶
  ¶
  // NewClient creates a new rpc client object that uses the multiplexer m for¶
  // communication the namespace n for decoding objects. If n is nil then the¶
  // global namespace is used.¶
  func NewClient(m *multiplexer.Multiplexer, n *registry.Namespace) Client {»¶
    return client{rpc.NewClient(m, n)}¶
  «}¶
  ¶
  // Client compliance
  {{range $s.Methods}}
    ¶
    {{if .Result.Type}}
      func (c client) {{.Name}}({{range .Call.Params}}{{.Name}} {{Call "Go.Type" .Type}}, {{end}}l log.Logger) (res {{Call "Go.Type" .Result.Type}}, err error) {»¶
        var val interface{}¶
        if val, err = c.Send(&{{.Call.Struct.Name}}{
          {{range $i, $p :=  .Call.Params}}
            {{if $i}}, {{end}}{{.Name}}: {{.Name}}
          {{end}}
        }); err == nil {»¶
          res = val.(*{{.Result.Struct.Name}}).value¶
        «} else {»¶
          log.Errorf(l, "{{$s.Name}} {{.Name}} failed with error: %v", err)¶
        «}¶
        return¶
      «}¶
    {{else}}
      func (c client) {{.Name}}({{range .Call.Params}}{{.Name}} {{Call "Go.Type" .Type}}, {{end}}l log.Logger) error {»¶
        _, err := c.Send(&{{.Call.Struct.Name}}{
          {{range $i, $p :=  .Call.Params}}
            {{if $i}}, {{end}}{{.Name}}: {{.Name}}
          {{end}}
        })¶
        return err¶
      «}¶
    {{end}}
  {{end}}
{{end}}
