{{/*
 * Copyright (C) 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */}}

{{/* ---- Includes ---- */}}
{{Include "cpp_common.tmpl"    }}

{{$ | Macro "gfx_api.h" | Format (Global "clang-format") | Write "gfx_api.h"}}

{{/*
-------------------------------------------------------------------------------
  Entry point.
-------------------------------------------------------------------------------
*/}}
{{define "gfx_api.h"}}
{{template "C++.GeneratedHeader"}}
¶
#ifndef GAPIR_GFX_API_H
#define GAPIR_GFX_API_H
¶
#include <gapic/target.h>
¶
#include <stdint.h>
¶
namespace gapir {
¶
class Interpreter;
¶
namespace gfxapi {
¶
// Register the API functions with the interpreter
extern void Register(Interpreter* interpreter);
¶
// Look-up the API function addresses for the currently bound context.
extern void Initialize();
¶
namespace Ids {
  // List of the function ids for the API functions. This list have to be consistent with the
  // function ids on the server side because they are part of the communication protocol
  {{range $i, $c := AllCommands $}}
    static const uint16_t {{Template "C++.Public" (Macro "CmdName" $c)}} = {{$i}};
  {{end}}
} // namespace FunctionIds
¶
  {{range $e := $.Enums}}
    {{Template "DeclareType" $e}}
  {{end}}
  {{range $p := $.Pseudonyms}}
    {{Template "DeclareType" $p}}
  {{end}}
  {{range $c := $.Classes}}
    {{Template "DeclareType" $c}}
  {{end}}
¶
{{range $c := AllCommands $}}
  {{if not (GetAnnotation $c "synthetic")}}
    {{Template "C++.TypedefFunctionPtr" $c}}
  {{end}}
{{end}}
¶
{{range $c := AllCommands $}}
  {{if not (GetAnnotation $c "synthetic")}}
    extern {{Template "C++.FunctionPtrDecl" $c}};
  {{end}}
{{end}}
¶
} // namespace gfxapi
} // namespace gapir
¶
#endif  // GAPIR_GFX_API_H
¶
{{end}}


{{/*
-------------------------------------------------------------------------------
  Emits the C++ type declaration specified AST type if it hasn't already been
  declared.
-------------------------------------------------------------------------------
*/}}
{{define "DeclareType"}}
  {{if not (IsBuiltin $)}}
    {{$key  := printf "ApiType%vDeclared" $.Name}}
    {{if not (Global $key)}}
      {{Global $key "true"}}
      {{     if IsEnum        $}}{{Template "DeclareEnum"      $}}
      {{else if IsPseudonym   $}}{{Template "DeclarePseudonym" $}}
      {{else if IsClass       $}}{{Template "DeclareClass"     $}}
      {{else if IsPointer     $}}{{Template "DeclareType"      $.To}}
      {{else if IsStaticArray $}}{{Template "DeclareType"      $.ValueType}}
      {{else}}{{Error "DeclareType does not support type '%T'" $}}
      {{end}}
    {{end}}
  {{end}}
{{end}}


{{/*
-------------------------------------------------------------------------------
  Emits an enum declaration as a number of uint32_t constants in a namespace.
-------------------------------------------------------------------------------
*/}}
{{define "DeclareEnum"}}
  {{AssertType $ "Enum"}}

  enum class {{Template "C++.EnumName" $}} : uint32_t {
    {{range $entry := $.Entries}}
      {{Template "C++.EnumEntryName" $entry}} = {{$entry.Value}},
    {{end}}
  };¶
{{end}}


{{/*
-------------------------------------------------------------------------------
  Emits the declaration of the pseudonym's target type (if it is not already
  declared) followed by the pseudonym's typedef declaration.
-------------------------------------------------------------------------------
*/}}
{{define "DeclarePseudonym"}}
  {{AssertType $ "Pseudonym"}}

  {{Template "DeclareType" $.To}}

  typedef {{Template "C++.Type" $.To}} {{Template "C++.Type" $}};
{{end}}


{{/*
-------------------------------------------------------------------------------
  Emits the declarations for all non-declared field types of the class followed
  by the class declaration.
-------------------------------------------------------------------------------
*/}}
{{define "DeclareClass"}}
  {{AssertType $ "Class"}}

  {{if not (GetAnnotation $ "internal")}}
    {{range $f := $.Fields}}
      {{Template "DeclareType" (TypeOf $f)}}
    {{end}}

    typedef {{if GetAnnotation $ "union"}}union{{else}}struct{{end}} {
      {{range $f := $.Fields}}
        {{Template "C++.Type" $f}} {{$f.Name}}{{Template "C++.ArrayPostfix" $f}};
      {{end}}
    } {{Template "C++.Type" $}};¶
  {{end}}
{{end}}
