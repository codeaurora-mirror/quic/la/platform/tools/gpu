/*
 * Copyright (C) 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef GAPIR_GAZER_LISTENER_H
#define GAPIR_GAZER_LISTENER_H

#include <memory>

namespace gapic {

class Connection;

} // namespace gapic


namespace gapir {

class ServerConnection;

// Class for listening to incoming connections from the server.
class ServerListener {
public:
    // Construct a ServerListener using the specified connection.
    // maxMemorySize is the maximum memory size that can be reported as
    // supported by this device.
    explicit ServerListener(std::unique_ptr<gapic::Connection> conn, uint64_t maxMemorySize);

    // Accept a new incoming connection on the underlying socket and creates a ServerConnection over
    // the newly created socket object.
    std::unique_ptr<ServerConnection> acceptConnection();

    enum ConnectionType {
        DEVICE_INFO      = 0,
        REPLAY_REQUEST   = 1,
        SHUTDOWN_REQUEST = 2,
    };

private:
    // The underlying server socket for the listener
    std::unique_ptr<gapic::Connection> mConn;
    // The maximum memory size that can be reported as supported by this device.
    uint64_t mMaxMemorySize;
};

}  // namespace gapir

#endif  // GAPIR_SOCKET_LISTENER_H
