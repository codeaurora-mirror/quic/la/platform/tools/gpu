{{/*
 * Copyright (C) 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */}}

{{/* ---- Includes ---- */}}
{{Include "api_classnames.tmpl"}}
{{Include "cpp_common.tmpl"    }}

{{/* ---- Overrides ---- */}}
{{Global "C++.EnumTypeOverride"   "uint32_t"}}
{{Global "C++.Statement.Override" "Statement"}}

{{$filename := print (Global "API") "_spy.h" }}
{{Global "SpyName" (print (Title (Global "API")) "Spy")}}
{{$ | Macro "Spy" | Format (Global "clang-format") | Write $filename}}

{{define "Spy"}}
  {{AssertType $ "API"}}
  {{Template "C++.AOSP.Copyright"}}
  {{$spyname := Global "SpyName"}}
  {{$guard := print "GAPII_" (Upper (Global "API")) "_SPY_H"}}
¶
  #ifndef {{$guard}}
  #define {{$guard}}
¶
  #include "{{Global "API"}}_imports.h"
  #include "{{Global "API"}}_types.h"
¶
  #include "spy_base.h"
¶
  #include <gapic/log.h>
  #include <gapic/coder/memory.h>
  #include <gapic/coder/atom.h>
  #include <gapic/coder/{{Global "API"}}.h>
¶
  #define __STDC_FORMAT_MACROS
  #include <inttypes.h>
¶
  #include <memory>
  #include <string>
¶
  using gapic::coder::atom::Observations;
¶
  namespace gapii {«
¶
  class {{$spyname}} : public SpyBase {
  «public:»
    inline void init(std::shared_ptr<gapic::Encoder> encoder);
¶
    {{ForEach (AllCommands $) "MethodDefinitions" | JoinWith "\n"}}
¶
  «protected:»
    {{Template "ApiClassnames.Imports"}} mImports;
¶
    // Globals
    {{range $g := $.Globals}}
      {{Template "C++.Type" $g}} {{$g.Name}};
    {{end}}
¶
    #include "{{Global "API"}}_state_externs.inl"
  };
¶
  // Inline methods
  inline void {{$spyname}}::init(std::shared_ptr<gapic::Encoder> encoder) {
    SpyBase::init(encoder);
    mImports.Resolve();
    {{range $g := $.Globals}}
      {{$g.Name}} = §
        {{if $g.Default}}
          {{Macro "C++.Read" $g.Default}};
        {{else}}
          {{Macro "C++.Null" (TypeOf $g)}};
        {{end}}
    {{end}}
  }
¶
  {{ForEach (AllCommands $) "MethodImplementation" | JoinWith "\n¶"}}
¶
  »} // namespace gapii
¶
  #endif // {{$guard}}
¶
{{end}}


{{/*
-------------------------------------------------------------------------------
  Emits the static variable name holding the ID of the given function.
-------------------------------------------------------------------------------
*/}}
{{define "ID"}}
  {{AssertType $ "Function"}}

  {{Template "CmdName" $ | SplitPascalCase | Upper | JoinWith "_"}}_ID
{{end}}


{{/*
-------------------------------------------------------------------------------
  Emits a single C++ method definition for the given atom.
-------------------------------------------------------------------------------
*/}}
{{define "MethodDefinitions"}}
  {{AssertType $ "Function"}}

  inline {{Template "C++.ReturnType" $}} {{Template "CmdName" $}}({{Template "C++.CallParameters" $}});
{{end}}


{{/*
-------------------------------------------------------------------------------
  Emits the possibly converted coder argument.
-------------------------------------------------------------------------------
*/}}
{{define "C++.CoderArgument"}}
  {{AssertType $ "Parameter"}}

  {{if IsPointer (Underlying $.Type)}}
    gapic::coder::{{Global "API"}}::{{Template "C++.TypeName" $.Type}}(gapic::coder::memory::Pointer(reinterpret_cast<uintptr_t>({{$.Name}}), 0))
  {{else}}{{$.Name}}{{end}}
{{end}}


{{/*
-------------------------------------------------------------------------------
  Emits the comma-separated list of argument names for the given function.
-------------------------------------------------------------------------------
*/}}
{{define "C++.CoderArguments"}}
  {{AssertType $ "Function"}}

  observations {{range $i, $p := $.FullParameters}}, {{Template "C++.CoderArgument" $p}}{{end}}
{{end}}


{{/*
-------------------------------------------------------------------------------
  Emits the inline method body for the given atom.
-------------------------------------------------------------------------------
*/}}
{{define "MethodImplementation"}}
  {{AssertType $ "Function"}}

  {{$name    := Macro "CmdName" $}}
  {{$spyname := Global "SpyName"}}

  inline {{Template "C++.ReturnType" $}} {{$spyname}}::{{$name}}({{Template "C++.CallParameters" $}}) {
    GAPID_INFO({{Template "C++.PrintfCommandCall" $}});
    {{if not (IsVoid $.Return.Type)}}¶
      {{Template "C++.ReturnType" $}} result = {{Template "C++.Null" (TypeOf $.Return)}};
    {{end}}
¶
    Observations observations;
    do {
      {{Global "CurrentCommand" $}}
      {{Template "C++.Block" $.Block}}
    } while(false);
    observe(observations.mWrites);
¶
    gapic::coder::{{Global "API"}}::{{Title .Name}} coder({{Template "C++.CoderArguments" $}});
    mEncoder->Object(&coder);

    {{if not (IsVoid $.Return.Type)}}¶
      return result;
    {{end}}
  }
{{end}}


{{/*
-------------------------------------------------------------------------------
  Emits the C++ encode logic for the specified parameter.
-------------------------------------------------------------------------------
*/}}
{{define "Encode"}}
  {{AssertType $.Type      "Type"}}
  {{AssertType $.Name      "string"}}
  {{AssertType $.Parameter "Parameter"}}

  {{     if IsPseudonym   $.Type}}{{Template "Encode" "Type" $.Type.To "Name" $.Name "Parameter" $.Parameter}}
  {{else if IsBool        $.Type}}mEncoder->Bool({{$.Name}});
  {{else if IsU8          $.Type}}mEncoder->Uint8({{$.Name}});
  {{else if IsS8          $.Type}}mEncoder->Int8({{$.Name}});
  {{else if IsU16         $.Type}}mEncoder->Uint16({{$.Name}});
  {{else if IsS16         $.Type}}mEncoder->Int16({{$.Name}});
  {{else if IsF32         $.Type}}mEncoder->Float32({{$.Name}});
  {{else if IsU32         $.Type}}mEncoder->Uint32({{$.Name}});
  {{else if IsS32         $.Type}}mEncoder->Int32({{$.Name}});
  {{else if IsF64         $.Type}}mEncoder->Float64({{$.Name}});
  {{else if IsU64         $.Type}}mEncoder->Uint64({{$.Name}});
  {{else if IsS64         $.Type}}mEncoder->Int64({{$.Name}});
  {{else if IsInt         $.Type}}mEncoder->Int64({{$.Name}});
  {{else if IsUint        $.Type}}mEncoder->Uint64({{$.Name}});
  {{else if IsString      $.Type}}mEncoder->String({{$.Name}});
  {{else if IsEnum        $.Type}}mEncoder->Uint32(static_cast<uint32_t>({{$.Name}}));
  {{else if IsPointer     $.Type}}mEncoder->Pointer({{$.Name}});
  {{else if IsStaticArray $.Type}}
    for (int i = 0; i < {{$.Type.Size}}; i++) {
      {{Template "Encode" "Type" $.Type.ValueType "Name" (print $.Name "[i]") "Parameter" $.Parameter}}
    }
  {{else if IsClass       $.Type}}
    mEncoder->Uint64(0); // CreatedAt
    {{range $f := $.Type.Fields}}
      {{Template "Encode" "Type" $f.Type "Name" (print $.Name ".m" $f.Name) "Parameter" $.Parameter}}
    {{end}}
  {{else}}{{Error "Encode passed unsupported type: %s" $.Type.Name}}
  {{end}}
{{end}}


{{/*
-------------------------------------------------------------------------------
  Override for the "C++.Statement" macro.
-------------------------------------------------------------------------------
*/}}
{{define "Statement"}}
  {{     if IsReturn $}}break;
  {{else if IsFence  $}}{{Template "Fence" $}}
  {{else if IsCopy   $}}{{Error "Copy statement found outside of a Fence"}}
  {{else              }}{{Template "C++.Statement.Default" $}}
  {{end}}
{{end}}


{{/*
-------------------------------------------------------------------------------
  An override for the "C++.Fence" macro.
-------------------------------------------------------------------------------
*/}}
{{define "Fence"}}
  {{AssertType $ "Fence"}}

  {{$c := Global "CurrentCommand"}}
  {{AssertType $c "Function"}}

  {{if not (IsNil $.Statement)}}
    {{if IsCopy $.Statement}}
      {{/* Apply the fenced-copy read */}}
      {{Template "C++.Type" $.Statement.Dst}} copy__dst__ = copy({{Template "C++.Read" $.Statement.Dst}}, {{Template "C++.Read" $.Statement.Src}});
    {{end}}
  {{end}}

  {{/* Observe all reads */}}
  observe(observations.mReads);

  {{/* Perform the call */}}
  {{if not (GetAnnotation $c "synthetic")}}
    {{if not (IsVoid $c.Return.Type)}}result = {{end}}mImports.{{Template "CmdName" $c}}({{Template "C++.CallArguments" $c}});
  {{end}}

  {{/* Perform the fenced statement */}}
  {{if not (IsNil $.Statement)}}
    {{if IsCopy $.Statement}}
      write(copy__dst__);
    {{else}}
      {{Template "C++.Statement" $.Statement}}
    {{end}}
  {{end}}
{{end}}
