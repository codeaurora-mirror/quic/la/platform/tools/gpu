# Android gpu tools
--
The android.googlesource.com/platform/tools/gpu project holds the code for the
android gpu inspection, debugging and profiling tools.

You can view the full documentation on godoc.org [here](http://godoc.org/android.googlesource.com/platform/tools/gpu.git).

Interesting points to start from are:
- The apic tool http://godoc.org/android.googlesource.com/platform/tools/gpu.git/api/apic
- The gpu debugging gapis http://godoc.org/android.googlesource.com/platform/tools/gpu.git/server
